# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Shinjo Park <kde@peremen.name>, 2014, 2015, 2017, 2018, 2019, 2020, 2021, 2022, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-05-31 00:39+0000\n"
"PO-Revision-Date: 2023-07-23 00:47+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 22.12.3\n"

#: package/contents/ui/Battery.qml:25
#, kde-format
msgid "%1% charging"
msgstr "%1% 충전 중"

#: package/contents/ui/Battery.qml:25
#, kde-format
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/Battery.qml:25
#, kde-format
msgid "No info"
msgstr "정보 없음"

#: package/contents/ui/Connectivity.qml:40
#, kde-format
msgid "Unknown"
msgstr "알 수 없음"

#: package/contents/ui/Connectivity.qml:50
#, kde-format
msgid "No signal"
msgstr "신호 없음"

#: package/contents/ui/DeviceDelegate.qml:31
#, fuzzy, kde-format
#| msgid "Paired device is unavailable"
#| msgid_plural "All paired devices are unavailable"
msgid "Virtual Monitor is not available"
msgstr "모든 연결된 장치에 접근할 수 없음"

#: package/contents/ui/DeviceDelegate.qml:64
#, kde-format
msgid "File Transfer"
msgstr "파일 전송"

#: package/contents/ui/DeviceDelegate.qml:65
#, kde-format
msgid "Drop a file to transfer it onto your phone."
msgstr "파일을 끌어다 놓으면 휴대폰으로 전송합니다."

#: package/contents/ui/DeviceDelegate.qml:103
#, kde-format
msgid "Virtual Display"
msgstr "가상 디스플레이"

#: package/contents/ui/DeviceDelegate.qml:107
#, kde-format
msgid "Remote device does not have a VNC client (eg. krdc) installed."
msgstr ""

#: package/contents/ui/DeviceDelegate.qml:111
#, kde-format
msgid "The krfb package is required on the local device."
msgstr ""

#: package/contents/ui/DeviceDelegate.qml:118
#, kde-format
msgid "Failed to create the virtual monitor."
msgstr ""

#: package/contents/ui/DeviceDelegate.qml:168
#, kde-format
msgctxt "Battery charge percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/DeviceDelegate.qml:190
#, kde-format
msgid "Please choose a file"
msgstr "파일을 선택하십시오"

#: package/contents/ui/DeviceDelegate.qml:199
#, kde-format
msgid "Share file"
msgstr "파일 공유"

#: package/contents/ui/DeviceDelegate.qml:214
#, kde-format
msgid "Send Clipboard"
msgstr "클립보드 보내기"

#: package/contents/ui/DeviceDelegate.qml:233
#, kde-format
msgid "Ring my phone"
msgstr "내 휴대폰 울리기"

#: package/contents/ui/DeviceDelegate.qml:251
#, kde-format
msgid "Browse this device"
msgstr "이 장치 탐색"

#: package/contents/ui/DeviceDelegate.qml:268
#, kde-format
msgid "SMS Messages"
msgstr "SMS 메시지"

#: package/contents/ui/DeviceDelegate.qml:289
#, kde-format
msgid "Remote Keyboard"
msgstr "원격 키보드"

#: package/contents/ui/DeviceDelegate.qml:310
#, kde-format
msgid "Notifications:"
msgstr "알림:"

#: package/contents/ui/DeviceDelegate.qml:318
#, kde-format
msgid "Dismiss all notifications"
msgstr "모든 알림 지우기"

#: package/contents/ui/DeviceDelegate.qml:365
#, kde-format
msgid "Reply"
msgstr "답장"

#: package/contents/ui/DeviceDelegate.qml:375
#, kde-format
msgid "Dismiss"
msgstr "끄기"

#: package/contents/ui/DeviceDelegate.qml:388
#, kde-format
msgid "Cancel"
msgstr "취소"

#: package/contents/ui/DeviceDelegate.qml:402
#, kde-format
msgctxt "@info:placeholder"
msgid "Reply to %1…"
msgstr "%1 님에게 답장…"

#: package/contents/ui/DeviceDelegate.qml:420
#, kde-format
msgid "Send"
msgstr "보내기"

#: package/contents/ui/DeviceDelegate.qml:446
#, kde-format
msgid "Run command"
msgstr "명령 실행"

#: package/contents/ui/DeviceDelegate.qml:454
#, kde-format
msgid "Add command"
msgstr "명령 추가"

#: package/contents/ui/FullRepresentation.qml:58
#, kde-format
msgid "No paired devices"
msgstr "연결된 장치 없음"

#: package/contents/ui/FullRepresentation.qml:58
#, kde-format
msgid "Paired device is unavailable"
msgid_plural "All paired devices are unavailable"
msgstr[0] "모든 연결된 장치에 접근할 수 없음"

#: package/contents/ui/FullRepresentation.qml:60
#, kde-format
msgid "Install KDE Connect on your Android device to integrate it with Plasma!"
msgstr "Plasma와 안드로이드 장치를 연결하려면 KDE Connect를 설치하십시오!"

#: package/contents/ui/FullRepresentation.qml:64
#, kde-format
msgid "Pair a Device..."
msgstr "장치 연결..."

#: package/contents/ui/FullRepresentation.qml:76
#, kde-format
msgid "Install from Google Play"
msgstr "Google Play에서 설치"

#: package/contents/ui/FullRepresentation.qml:86
#, kde-format
msgid "Install from F-Droid"
msgstr "F-Droid에서 설치"

#: package/contents/ui/main.qml:59
#, kde-format
msgid "KDE Connect Settings..."
msgstr "KDE Connect 설정..."

#~ msgid "Save As"
#~ msgstr "다른 이름으로 저장"

#~ msgid "Take a photo"
#~ msgstr "사진 찍기"

#~ msgid "Configure..."
#~ msgstr "설정..."

#~ msgctxt ""
#~ "Display the battery charge percentage with the label \"Battery:\" so the "
#~ "user knows what is being displayed"
#~ msgid "Battery: %1"
#~ msgstr "배터리: %1"

#~ msgid "%1 (%2)"
#~ msgstr "%1(%2)"

#~ msgid "Share text"
#~ msgstr "텍스트 공유"

#~ msgid "Charging: %1%"
#~ msgstr "충전 중: %1%"

#~ msgid "Discharging: %1%"
#~ msgstr "방전 중: %1%"
